package apps.scvh.com.deviantapi.frontend.activities.settings.filters;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Фильтр для настройки количества выводимых картинок за раз
 * Девиантарт не позволяет вывести больше 50 картинок и меньше 1
 */
public class SettingsOffsetFilter implements InputFilter {

    //пусть будет тут
    private final int MAX = 50;
    private final int MIN = 1;

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
                               int dend) {
        try {
            //Это нагло нагуглено на стек оверфлоу
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(MIN, MAX, input)) {
                return null;
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }

}
