package apps.scvh.com.deviantapi.depinj.searchinjection;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.helpers.BigImageLoaderProxy;
import apps.scvh.com.deviantapi.controllers.helpers.ImageLoader;
import apps.scvh.com.deviantapi.controllers.viewloaders.AdapterWorker;
import apps.scvh.com.deviantapi.core.DeviantSearchManager;
import apps.scvh.com.deviantapi.core.api.DeviantApiWorker;
import apps.scvh.com.deviantapi.core.api.DeviantRetrofit;
import apps.scvh.com.deviantapi.core.token.ApiKeyProvider;
import apps.scvh.com.deviantapi.frontend.search.SearchFacade;
import apps.scvh.com.deviantapi.frontend.search.workers.SearchPullRefresher;
import apps.scvh.com.deviantapi.frontend.search.workers.SearchWorker;
import apps.scvh.com.deviantapi.frontend.searchlist.DeviantSearchAdapter;
import apps.scvh.com.deviantapi.system.NetworkConnectivityManager;
import apps.scvh.com.deviantapi.system.SettingsManager;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class SearchInjectorModule {

    private Context context;

    public SearchInjectorModule(Context context) {
        this.context = context;
    }

    @Provides
    DeviantSearchManager searchManager(DeviantApiWorker apiWorker) {
        return new DeviantSearchManager(apiWorker);
    }

    @Provides
    DeviantApiWorker apiWorker(DeviantRetrofit retrofit, ApiKeyProvider keyProvider,
                               SettingsManager settingsManager) {
        return new DeviantApiWorker(retrofit, keyProvider, settingsManager);
    }

    @Provides
    ApiKeyProvider keyProvider() {
        return new ApiKeyProvider(context);
    }

    @Provides
    DeviantRetrofit retrofitObject() {
        Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.deviant_url))
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build().create(DeviantRetrofit.class);
    }

    @Provides
    DeviantSearchAdapter adapter(ImageLoader loader, BigImageLoaderProxy loaderProxy) {
        return new DeviantSearchAdapter(loader, loaderProxy);
    }

    @Provides
    BigImageLoaderProxy loaderProxy() {
        return new BigImageLoaderProxy(context);
    }

    @Provides
    ImageLoader loader() {
        return new ImageLoader(context);
    }

    @Provides
    AdapterWorker adapterWorker() {
        return new AdapterWorker(context);
    }

    @Provides
    SettingsManager settingsManager() {
        return new SettingsManager(context);
    }

    @Provides
    NetworkConnectivityManager connectivityManager() {
        return new NetworkConnectivityManager(context);
    }

    @Provides
    SearchFacade searchFacade(DeviantSearchManager searchManager, NetworkConnectivityManager
            connectivityManager, AdapterWorker worker, DeviantSearchAdapter searchAdapter,
                              SettingsManager settingsManager) {
        return new SearchFacade(new SearchPullRefresher(searchManager, connectivityManager, worker, searchAdapter, context, settingsManager),
                new SearchWorker(searchManager, connectivityManager, worker, searchAdapter, context, settingsManager));
    }
}
