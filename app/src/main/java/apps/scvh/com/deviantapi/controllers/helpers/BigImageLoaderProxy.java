package apps.scvh.com.deviantapi.controllers.helpers;


import android.content.Context;
import android.content.Intent;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.core.abstractions.DeviantImage;
import apps.scvh.com.deviantapi.frontend.activities.DeviantBigImage;
import io.reactivex.Observable;

/**
 * Класс загружающий активити с картинкой при клике.
 */
public class BigImageLoaderProxy {

    private Context context;

    public BigImageLoaderProxy(Context context) {
        this.context = context;
    }

    public void gotoBigImage(Observable<DeviantImage> image) {
        image.subscribe(deviantImage -> {
            Intent intent = new Intent(context, DeviantBigImage.class);
            intent.putExtra(context.getString(R.string.data_flag), deviantImage);
            context.startActivity(intent);
        });
    }
}
