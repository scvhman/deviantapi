package apps.scvh.com.deviantapi.controllers.viewloaders;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.helpers.ImageLoader;
import apps.scvh.com.deviantapi.core.abstractions.DeviantImage;
import apps.scvh.com.deviantapi.frontend.activities.DeviantBigImage;
import apps.scvh.com.deviantapi.system.TimeConverter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Класс занимающийся "загрузкой" картинки в активити при клике
 */
public class BigImageViewsHolder {

    //Просто блок вьюх.
    //Это все инжектится через butterknife
    @BindView(R.id.deviant_big_picture)
    ImageView picture;
    @BindView(R.id.image_name)
    TextView imageName;
    @BindView(R.id.image_category)
    TextView imageCategory;
    @BindView(R.id.image_date)
    TextView imageDate;
    @BindView(R.id.author_username)
    TextView authorUsername;
    @BindView(R.id.author_picture)
    ImageView authorPicture;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.comments)
    TextView comments;

    private ImageLoader imageLoader;
    private TimeConverter timeConverter;
    private Context context;

    public BigImageViewsHolder(DeviantBigImage bigImage, ImageLoader imageLoader, TimeConverter
            timeConverter) {
        this.imageLoader = imageLoader;
        this.timeConverter = timeConverter;
        context = bigImage; //топ преобразование, если тебе интересно почему я передал сюда
        // активити, а не просто контектс посмотри классы для DI
        ButterKnife.bind(this, bigImage);
    }

    /**
     * Собственно сама внедрялка данных в вьюхи
     * @param imageObservable данные о картинке
     */
    public void initBigPicture(Observable<DeviantImage> imageObservable) {
        imageObservable.observeOn(AndroidSchedulers.mainThread()).subscribe(image -> {
            imageLoader.fastLoad(Observable.just(image.getContent().getSrc()), Observable.just(picture));
            imageName.setText(image.getTitle());
            if (image.getCategory() != null) {
                imageCategory.setText(image.getCategory());
            }
            timeConverter.convertTimestampToString(image.getTime()).observeOn(AndroidSchedulers.mainThread()).subscribe(date -> imageDate.setText(date));
            likes.setText(String.format(context.getString(R.string.likes), image.getStats().getFavourites()));
            comments.setText(String.format(context.getString(R.string.comments), image.getStats().getComments()));
            authorUsername.setText(image.getAuthor().getUsername());
            setLinkForAuthor(image.getAuthor().getUsername());
            imageLoader.fastLoad(Observable.just(image.getAuthor().getUsericon()), Observable.just(authorPicture));
        });
    }

    /**
     * Делает ссылочку кликабельной
     */
    private void setLinkForAuthor(String name) {
        RxView.clicks(authorUsername).subscribe(click -> {
            Intent deviantArtPageIntent = new Intent(Intent.ACTION_VIEW);
            deviantArtPageIntent.setData(Uri.parse(String.format(context.getString(R.string
                    .author_url), name)));
            context.startActivity(deviantArtPageIntent);
        });
    }

}
