package apps.scvh.com.deviantapi.controllers.viewloaders;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import apps.scvh.com.deviantapi.core.abstractions.SearchResult;
import apps.scvh.com.deviantapi.frontend.searchlist.DeviantSearchAdapter;


/**
 * Более удобная работа с адаптером
 */
public class AdapterWorker {

    private Context context;

    public AdapterWorker(Context context) {
        this.context = context;
    }

    /**
     * Добавление новых картинок в адаптер
     * @param result  результат поиска
     * @param adapter адаптер
     */
    public void addImagesToAdapter(SearchResult result, DeviantSearchAdapter adapter, RecyclerView view) {
        if (adapter.getImages().size() == 0) {
            adapter.addNewImages(result.getResults());
            recyclerInit(adapter, view, context);
        } else {
            adapter.addNewImages(result.getResults());
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Выпиливает все картинки из recyclerview и обновляет его
     */
    public void cleanUpSearchResults(DeviantSearchAdapter adapter) {
        adapter.cleanUp();
        adapter.notifyDataSetChanged();
    }

    /**
     * Ставить рецайклеру адаптер во время первого поиска
     */
    private void recyclerInit(DeviantSearchAdapter adapter, RecyclerView recyclerView, Context context) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }
}
