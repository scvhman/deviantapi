package apps.scvh.com.deviantapi.frontend.activities.settings;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceFragment;
import android.text.InputFilter;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.frontend.activities.settings.filters.SettingsKeyFilter;
import apps.scvh.com.deviantapi.frontend.activities.settings.filters.SettingsOffsetFilter;


public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        initFilters();
    }

    private void initFilters() {
        ((EditTextPreference) findPreference(getString(R.string.offset_key))).getEditText().setFilters(new InputFilter[]{new SettingsOffsetFilter()});
        ((EditTextPreference) findPreference(getString(R.string.api_key))).getEditText().setFilters(new InputFilter[]{new SettingsKeyFilter()});

    }
}
