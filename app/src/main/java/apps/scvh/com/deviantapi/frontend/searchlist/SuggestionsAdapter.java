package apps.scvh.com.deviantapi.frontend.searchlist;


import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.frontend.search.SearchAdapter;

public class SuggestionsAdapter extends CursorAdapter {

    //Нужно чисто для того чтобы при нажимании на вьюху производился поиск
    private SearchAdapter adapter;

    public SuggestionsAdapter(Context context, Cursor c, int flags, SearchAdapter adapter) {
        super(context, c, flags);
        this.adapter = adapter;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.deviant_suggestion, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView suggestion = (TextView) view.findViewById(R.id.suggestion);
        String text = cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.suggestions_map)));
        suggestion.setText(text);
        clickInit(suggestion, text);
    }

    private void clickInit(View view, String suggestion) {
        RxView.clicks(view).subscribe(click -> {
            adapter.search(suggestion);
        });
    }
}
