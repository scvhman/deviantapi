package apps.scvh.com.deviantapi.controllers.helpers;


import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import io.reactivex.Observable;

/**
 * Простая загружалка картинок в вьюхи. Работает через пикассо
 */
public class ImageLoader {

    private Context context;

    public ImageLoader(Context context) {
        this.context = context;
    }


    /**
     * @param query ссылка на картинку
     * @param view  imageview
     */
    public void fastLoad(Observable<String> query, Observable<ImageView> view) {
        //цiкавий трiк. Чисто теоретично я мав би скомпонувати кверi i вью в якийсь еррей i
        // зробити з ними щось в субскрайбi, але я не буду це робити
        Observable.zip(query, view, (q, v) -> {
            if (q != null) {
                Picasso.with(context).load(q).into(v);
            }
            return q;
        }).subscribe();
    }

}
