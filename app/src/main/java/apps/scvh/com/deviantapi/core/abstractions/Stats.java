package apps.scvh.com.deviantapi.core.abstractions;


import java.io.Serializable;

public class Stats implements Serializable {

    private int comments;
    private int favourites;

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getFavourites() {
        return favourites;
    }

    public void setFavourites(int favourites) {
        this.favourites = favourites;
    }
}
