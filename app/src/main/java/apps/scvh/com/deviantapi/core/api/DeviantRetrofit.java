package apps.scvh.com.deviantapi.core.api;


import apps.scvh.com.deviantapi.core.abstractions.SearchResult;
import apps.scvh.com.deviantapi.core.abstractions.SearchSuggestions;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Дефолтный интерфейс ретрофита. Нужно чтобы делать вызовы к апи
 */
public interface DeviantRetrofit {

    @GET("browse/tags")
    Call<SearchResult> getSearch(@Query("tag") String search, @Query("offset") int offset, @Query
            ("limit") int limit, @Query("access_token") String token);

    @GET("browse/tags/search")
    Call<SearchSuggestions> getSuggestion(@Query("tag_name") String suggest, @Query
            ("access_token") String token);
}
