package apps.scvh.com.deviantapi.depinj;


import apps.scvh.com.deviantapi.depinj.imageinjection.DaggerImageInjectorComponent;
import apps.scvh.com.deviantapi.depinj.imageinjection.ImageInjectorModule;
import apps.scvh.com.deviantapi.depinj.searchinjection.DaggerSearchInjectorComponent;
import apps.scvh.com.deviantapi.depinj.searchinjection.SearchInjectorModule;
import apps.scvh.com.deviantapi.frontend.activities.DeviantBigImage;
import apps.scvh.com.deviantapi.frontend.activities.DeviantSearch;


/**
 * Просто небольшой класс для быстрого DI
 */
public class Injector {

    public static void inject(DeviantSearch activity) {
        DaggerSearchInjectorComponent.builder().searchInjectorModule(new SearchInjectorModule(activity)).build().inject(activity);
    }

    public static void inject(DeviantBigImage activity) {
        DaggerImageInjectorComponent.builder().imageInjectorModule(new ImageInjectorModule(activity)).build().inject(activity);
    }
}
