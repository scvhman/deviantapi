package apps.scvh.com.deviantapi.frontend.searchlist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.ArrayList;
import java.util.List;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.helpers.BigImageLoaderProxy;
import apps.scvh.com.deviantapi.controllers.helpers.ImageLoader;
import apps.scvh.com.deviantapi.core.abstractions.DeviantImage;
import io.reactivex.Observable;

public class DeviantSearchAdapter extends RecyclerView.Adapter<DeviantSearchAdapter.ViewHolder> {

    private ArrayList<DeviantImage> images;
    private ImageLoader imageLoader;
    private BigImageLoaderProxy loaderProxy;

    public DeviantSearchAdapter(ImageLoader imageLoader, BigImageLoaderProxy loaderProxy) {
        this.imageLoader = imageLoader;
        this.loaderProxy = loaderProxy;
        images = new ArrayList<>();
    }

    public void addNewImages(List<DeviantImage> newImages) {
        images.addAll(newImages);
    }

    public ArrayList<DeviantImage> getImages() {
        return images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.deviant_picture, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (images.get(position).getContent() != null) {
            initHolder(holder, images.get(position).getContent().getSrc());
            initClickListener(holder, images.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    private void initHolder(DeviantSearchAdapter.ViewHolder holder, String url) {
        imageLoader.fastLoad(Observable.just(url), Observable.just(holder.deviantPicture));
    }

    private void initClickListener(DeviantSearchAdapter.ViewHolder holder, DeviantImage image) {
        RxView.clicks(holder.card).subscribe(cons -> {
            loaderProxy.gotoBigImage(Observable.just(image));
        });
    }

    /**
     * Для выпиливания картинок из листа
     */
    public void cleanUp() {
        images.clear();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView deviantPicture;
        CardView card;

        ViewHolder(View itemView) {
            super(itemView);
            deviantPicture = (ImageView) itemView.findViewById(R.id.picture_from_search);
            card = (CardView) itemView.findViewById(R.id.list_card);
        }

    }
}
