package apps.scvh.com.deviantapi.core.abstractions;

import java.io.Serializable;

public class DeviantImage implements Serializable {

    private String deviationid;
    private String title;
    private String category;
    private long published_time;
    private Picture preview;
    private Picture content;
    private Author author;
    private Stats stats;

    public String getDeviationid() {
        return deviationid;
    }

    public void setDeviationid(String deviationid) {
        this.deviationid = deviationid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getTime() {
        return published_time;
    }

    public void setTime(long time) {
        this.published_time = time;
    }

    public Picture getPreview() {
        return preview;
    }

    public void setPreview(Picture preview) {
        this.preview = preview;
    }

    public Picture getContent() {
        return content;
    }

    public void setContent(Picture content) {
        this.content = content;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }
}
