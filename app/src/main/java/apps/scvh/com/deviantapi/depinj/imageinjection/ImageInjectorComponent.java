package apps.scvh.com.deviantapi.depinj.imageinjection;

import javax.inject.Singleton;

import apps.scvh.com.deviantapi.frontend.activities.DeviantBigImage;
import dagger.Component;

@Component(modules = {ImageInjectorModule.class})
@Singleton
public interface ImageInjectorComponent {
    void inject(DeviantBigImage bigImage);
}
