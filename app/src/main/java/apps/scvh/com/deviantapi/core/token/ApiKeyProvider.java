package apps.scvh.com.deviantapi.core.token;

import android.content.Context;
import android.preference.PreferenceManager;

import apps.scvh.com.deviantapi.R;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Инсекьюрно, да, но я не парился т.к это ТЗ-шка. По идее надо было сделать сохранение в шифрованную базу sqlite, либо
 * в secure storage, но я сделал так. За это сильно не бить
 */
public class ApiKeyProvider {

    private Context context;

    public ApiKeyProvider(Context context) {
        this.context = context;
    }

    public Observable<String> getApiKey() {
        return Observable.just(PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.api_key), ""))
                .subscribeOn(Schedulers.newThread());
    }

}
