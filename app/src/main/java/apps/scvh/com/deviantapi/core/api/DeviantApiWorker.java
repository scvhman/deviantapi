package apps.scvh.com.deviantapi.core.api;


import apps.scvh.com.deviantapi.core.abstractions.SearchResult;
import apps.scvh.com.deviantapi.core.abstractions.SearchSuggestions;
import apps.scvh.com.deviantapi.core.token.ApiKeyProvider;
import apps.scvh.com.deviantapi.system.SettingsManager;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Собственно класс в котором происходит вся магия связанная с получением контента с девиантарта
 */
public class DeviantApiWorker {

    private DeviantRetrofit retrofit;
    private ApiKeyProvider keyProvider;
    private SettingsManager settingsManager;


    public DeviantApiWorker(DeviantRetrofit retrofit, ApiKeyProvider keyProvider, SettingsManager
            settingsManager) {
        this.retrofit = retrofit;
        this.keyProvider = keyProvider;
        this.settingsManager = settingsManager;
    }

    /**
     * Получение результатов поиска
     * @param query  параметры поиска
     * @param offset "оффсет"
     * @return результаты поиска
     */
    public Observable<SearchResult> search(Observable<String> query, Observable<Integer> offset) {
        return Observable.zip(settingsManager.getNumberOfPicturesToLoad(), keyProvider.getApiKey(), query, offset, (settings, key, q, o) -> {
            SearchResult result = retrofit.getSearch(q, o, settings, key).execute().body();
            if (result != null) {
                return result;
            } else {
                return new SearchResult();
            }
        });
    }

    /**
     * Получение "предложений по поиску"
     * @param query параметры поиска
     * @return результаты
     */
    public Observable<SearchSuggestions> suggest(Observable<String> query) {
        return Observable.zip(keyProvider.getApiKey(), query, (key, q) -> {
            SearchSuggestions suggestions = retrofit.getSuggestion(q, key).execute().body();
            if (suggestions != null) {
                return suggestions;
            } else {
                return new SearchSuggestions();
            }
        }).subscribeOn(Schedulers.newThread());
    }
}
