package apps.scvh.com.deviantapi.frontend.search.workers;


import android.content.Context;
import android.database.MatrixCursor;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import java.util.Iterator;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.viewloaders.AdapterWorker;
import apps.scvh.com.deviantapi.core.DeviantSearchManager;
import apps.scvh.com.deviantapi.core.abstractions.SearchSuggestions;
import apps.scvh.com.deviantapi.frontend.search.SearchAdapter;
import apps.scvh.com.deviantapi.frontend.searchlist.DeviantSearchAdapter;
import apps.scvh.com.deviantapi.frontend.searchlist.SuggestionsAdapter;
import apps.scvh.com.deviantapi.system.NetworkConnectivityManager;
import apps.scvh.com.deviantapi.system.SettingsManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SearchWorker implements SearchView.OnQueryTextListener {

    private DeviantSearchManager searchHelper;
    private NetworkConnectivityManager connectivityManager;
    private AdapterWorker worker;
    private DeviantSearchAdapter adapter;
    private Context context;
    private SettingsManager settingsManager;

    private RecyclerView recyclerView;
    private SearchView searchView;

    //Аргументики
    public SearchWorker(DeviantSearchManager searchHelper, NetworkConnectivityManager
            connectivityManager, AdapterWorker worker, DeviantSearchAdapter adapter, Context
                                context, SettingsManager settingsManager) {
        this.searchHelper = searchHelper;
        this.connectivityManager = connectivityManager;
        this.worker = worker;
        this.adapter = adapter;
        this.context = context;
        this.settingsManager = settingsManager;
    }

    public void initSearch(RecyclerView recyclerView, SearchView searchView) {
        this.recyclerView = recyclerView;
        this.searchView = searchView;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        settingsManager.isApiKeyPresent().observeOn(AndroidSchedulers.mainThread()).subscribe(isPresent -> {
            if (isPresent) {
                handleSearch(query);
            } else {
                Toast.makeText(context, context.getString(R.string.no_key), Toast.LENGTH_SHORT).show();
            }
        });
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Observable.zip(settingsManager.getSuggestionsEnabled(), settingsManager.isApiKeyPresent(), (suggestions, api) -> suggestions && api).observeOn(AndroidSchedulers.mainThread()).subscribe(okay -> {
            if (!newText.equals("")) {
                handleSuggestions(newText);
            }
        });
        return true;
    }

    public void handleSearch(String search) {
        connectivityManager.checkConnectivityAndRun(() -> searchHelper.search(search).observeOn
                (AndroidSchedulers.mainThread()).subscribe(searchResult -> {
            worker.cleanUpSearchResults(adapter); //Чистим старые резульаты поиска
            if (searchResult.getResults().size() != 0) {
                worker.addImagesToAdapter(searchResult, adapter, recyclerView);
                //Добавляем новые в адаптер
                //Ставим offset для того чтобы скроллить картинки
                searchHelper.setHasNext(Boolean.valueOf(searchResult.getHasMore()));
                searchHelper.updateOffset(Observable.just(searchResult.getNextOffset()));
            } else {
                Toast.makeText(context, context.getString(R.string.no_found), Toast
                        .LENGTH_SHORT).show();
            }
        }));
    }

    private void handleSuggestions(String text) {
        searchHelper.getSuggestions(text).observeOn(AndroidSchedulers.mainThread()).subscribe
                (suggested -> {
                    if (suggested.getResults() != null) {
                        Iterator<SearchSuggestions.Suggested> iterator = suggested.getResults().iterator(); //Получаем итератор с результатами поиска
                        int position = 1;
                        MatrixCursor cursor = new MatrixCursor(new String[]{context.getString(R
                                .string.suggestions_key), context.getString(R.string
                                .suggestions_map)}); //Делаем курсом
                        while (iterator.hasNext()) { //Добавляем результаты поиска в курсор
                            cursor.addRow(new String[]{String.valueOf(position), iterator.next().getTagName()});
                            position++;
                        }
                        searchView.setSuggestionsAdapter(new SuggestionsAdapter(context, cursor, 0, new SearchAdapter(this))); //Делаем предложения по поиску для searchview
                    }
                });
    }
}
