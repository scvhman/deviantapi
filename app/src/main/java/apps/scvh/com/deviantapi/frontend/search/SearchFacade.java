package apps.scvh.com.deviantapi.frontend.search;


import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import apps.scvh.com.deviantapi.frontend.search.workers.SearchPullRefresher;
import apps.scvh.com.deviantapi.frontend.search.workers.SearchWorker;

/**
 * Небольшой фасад для инициализации всех этих ништяков
 */
public class SearchFacade {

    private SearchPullRefresher pullRefresher;
    private SearchWorker inputListener;

    public SearchFacade(SearchPullRefresher pullRefresher, SearchWorker inputListener) {
        this.pullRefresher = pullRefresher;
        this.inputListener = inputListener;
    }

    public void initSearch(SwipyRefreshLayout refreshLayout, RecyclerView recyclerView,
                           SearchView searchView) {
        inputListener.initSearch(recyclerView, searchView);
        pullRefresher.autoPullInit(refreshLayout, recyclerView);
        searchView.setOnQueryTextListener(inputListener);
    }
}
