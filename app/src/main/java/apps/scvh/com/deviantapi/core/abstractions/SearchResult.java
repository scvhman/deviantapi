package apps.scvh.com.deviantapi.core.abstractions;


import java.io.Serializable;
import java.util.List;

public class SearchResult implements Serializable {

    private int next_offset;
    private String has_more;
    private List<DeviantImage> results;

    public int getNextOffset() {
        return next_offset;
    }

    public void setNextOffset(int nextOffset) {
        this.next_offset = nextOffset;
    }

    public String getHasMore() {
        return has_more;
    }

    public void setHasMore(String hasMore) {
        this.has_more = hasMore;
    }

    public List<DeviantImage> getResults() {
        return results;
    }

    public void setResults(List<DeviantImage> results) {
        this.results = results;
    }
}
