package apps.scvh.com.deviantapi.frontend.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.viewloaders.BigImageViewsHolder;
import apps.scvh.com.deviantapi.core.abstractions.DeviantImage;
import apps.scvh.com.deviantapi.depinj.Injector;
import apps.scvh.com.deviantapi.system.NetworkConnectivityManager;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DeviantBigImage extends AppCompatActivity {

    @Inject
    BigImageViewsHolder viewsHolder;

    @Inject
    NetworkConnectivityManager connectivityManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deviant_image);
        Injector.inject(this);
        connectivityManager.checkConnectivityAndRun(() -> viewsHolder.initBigPicture(Observable
                .just((DeviantImage) getIntent()
                .getSerializableExtra(getString(R.string.data_flag))).subscribeOn(Schedulers
                        .newThread())));

    }
}
