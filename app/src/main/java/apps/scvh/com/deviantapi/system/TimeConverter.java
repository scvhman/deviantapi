package apps.scvh.com.deviantapi.system;


import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import apps.scvh.com.deviantapi.R;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Класс делающий из юниксового таймстампа строку
 */
public class TimeConverter {

    private Context context;

    public TimeConverter(Context context) {
        this.context = context;
    }

    public Observable<String> convertTimestampToString(long timestamp) {
        return Observable.defer(() -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat(context.getString(R.string
                    .timestamp), Locale.ENGLISH);
            dateFormat.setTimeZone(TimeZone.getDefault());
            return Observable.just(dateFormat.format(new Date(timestamp * 1000L)));
        }).subscribeOn(Schedulers.newThread());
    }
}
