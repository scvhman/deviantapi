package apps.scvh.com.deviantapi.system;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import apps.scvh.com.deviantapi.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Менеджер сети
 */
public class NetworkConnectivityManager {

    private Context context;

    public NetworkConnectivityManager(Context context) {
        this.context = context;
    }

    /**
     * @return Состояние подключения к сети
     */
    private Observable<Boolean> isThereInternetConnectivity() {
        return Observable.defer(() -> {
            //Трохи коду  з кон. менеджером який я пiдглянув в гуглодокументацii
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return Observable.just(activeNetwork != null && activeNetwork.isConnectedOrConnecting());
        }).subscribeOn(Schedulers.newThread());
    }

    /**
     * Небольшой метод который проверяет наличие сети и выполняет runnable, если сеть доступна
     * Насколько это круто с точки зрения архитектуры - решать тебе, но мне такая штучка нравится
     * ИМХО достаточно лаконично
     * @param runnable с кодом который надо выполнить
     */
    public void checkConnectivityAndRun(Runnable runnable) {
        isThereInternetConnectivity().observeOn(AndroidSchedulers.mainThread()).subscribe(isThere -> {
            if (isThere) {
                runnable.run();
            } else {
                Toast.makeText(context, context.getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
