package apps.scvh.com.deviantapi.frontend.search;


import apps.scvh.com.deviantapi.frontend.search.workers.SearchWorker;

/**
 * Небольшой адаптер чтобы при нажатии на вьюху в предложениях по поиску делался поиск
 */
public class SearchAdapter {

    private SearchWorker searchWorker;

    public SearchAdapter(SearchWorker searchWorker) {
        this.searchWorker = searchWorker;
    }

    public void search(String query) {
        searchWorker.handleSearch(query);
    }
}
