package apps.scvh.com.deviantapi.core.abstractions;


import java.util.List;

public class SearchSuggestions {

    private List<Suggested> results;

    public List<Suggested> getResults() {
        return results;
    }

    public void setResults(List<Suggested> results) {
        this.results = results;
    }

    //Внутрiшнiй класс, щоб не було многофайлов
    public class Suggested {

        private String tag_name;

        public String getTagName() {
            return tag_name;
        }

        public void setTagName(String tag_name) {
            this.tag_name = tag_name;
        }
    }
}
