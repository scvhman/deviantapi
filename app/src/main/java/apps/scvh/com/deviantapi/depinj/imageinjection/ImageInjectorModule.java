package apps.scvh.com.deviantapi.depinj.imageinjection;

import javax.inject.Singleton;

import apps.scvh.com.deviantapi.controllers.helpers.ImageLoader;
import apps.scvh.com.deviantapi.controllers.viewloaders.BigImageViewsHolder;
import apps.scvh.com.deviantapi.frontend.activities.DeviantBigImage;
import apps.scvh.com.deviantapi.system.NetworkConnectivityManager;
import apps.scvh.com.deviantapi.system.TimeConverter;
import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class ImageInjectorModule {

    /**
     * Почему активити, а не контекст:
     * Butteknif'у который я юзаю для инжекта вьюх нужно передать именно активити, а не контекст
     */
    private DeviantBigImage activity;

    public ImageInjectorModule(DeviantBigImage activity) {
        this.activity = activity;
    }

    @Provides
    BigImageViewsHolder viewsHolder(TimeConverter converter) {
        return new BigImageViewsHolder(activity, new ImageLoader(activity), converter);
    }

    @Provides
    TimeConverter timeConverter() {
        return new TimeConverter(activity);
    }

    @Provides
    NetworkConnectivityManager connectivityManager() {
        return new NetworkConnectivityManager(activity);
    }
}
