package apps.scvh.com.deviantapi.depinj.searchinjection;

import javax.inject.Singleton;

import apps.scvh.com.deviantapi.frontend.activities.DeviantSearch;
import dagger.Component;

@Singleton
@Component(modules = {SearchInjectorModule.class})
public interface SearchInjectorComponent {
    void inject(DeviantSearch activity);
}
