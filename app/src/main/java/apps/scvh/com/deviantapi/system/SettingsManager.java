package apps.scvh.com.deviantapi.system;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import apps.scvh.com.deviantapi.R;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Менеджер для настроек
 */
public class SettingsManager {

    private Context context;
    private SharedPreferences manager;

    public SettingsManager(Context context) {
        this.context = context;
        manager = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * @return Количество картинок которые нужно вывести за один раз
     */
    public Observable<Integer> getNumberOfPicturesToLoad() {
        //ведро сохраняет это как стринг, а не как int
        return Observable.just(Integer.valueOf(manager.getString(context.getString(R.string
                .offset_key), String
                .valueOf(10)))).subscribeOn(Schedulers.newThread());
    }

    /**
     * @return Штучка чекающая нужно ли выводить "предложения" по поиску или нет
     */
    public Observable<Boolean> getSuggestionsEnabled() {
        return Observable.defer(() -> Observable.just(manager.getBoolean(context.getString(R
                .string.suggestions_settings_key), true))).subscribeOn(Schedulers.newThread());
    }

    /**
     * Проверка на наличие ключа
     */
    public Observable<Boolean> isApiKeyPresent() {
        return Observable.defer(() -> {
            if (manager.getString(context.getString(R.string.api_key), "").equals("")) {
                return Observable.just(false);
            } else {
                return Observable.just(true);
            }
        }).subscribeOn(Schedulers.newThread());
    }
}
