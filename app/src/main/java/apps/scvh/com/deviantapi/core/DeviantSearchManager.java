package apps.scvh.com.deviantapi.core;

import apps.scvh.com.deviantapi.core.abstractions.SearchResult;
import apps.scvh.com.deviantapi.core.abstractions.SearchSuggestions;
import apps.scvh.com.deviantapi.core.api.DeviantApiWorker;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Небольшой класс работающий враппером над всем апи.
 * Олсо, реализация скроллинга картинок "страничками" тоже здесь
 */

public class DeviantSearchManager {

    private DeviantApiWorker apiWorker;

    //Это все для получения картинок во время скроллинга
    private int offsetHolder;
    private String queryHolder;
    private boolean hasNext;

    public DeviantSearchManager(DeviantApiWorker apiWorker) {
        this.apiWorker = apiWorker;
        offsetHolder = 0;
        queryHolder = "";
    }

    /**
     * Поиск по девианту
     * @param query поимковый запрос
     * @return результаты поиска
     */
    public Observable<SearchResult> search(String query) {
        offsetHolder = 0;
        queryHolder = query;
        return apiWorker.search(Observable.just(query), Observable.just(0));
    }

    /**
     * Получение следующих n картинок.
     * @return новые картинки
     */
    public Observable<SearchResult> updateSearchResults() {
        return apiWorker.search(Observable.just(queryHolder), Observable.just(offsetHolder));
    }

    /**
     * Предложения по поиску
     * @param query запрос
     * @return результаты
     */
    public Observable<SearchSuggestions> getSuggestions(String query) {
        return apiWorker.suggest(Observable.just(query)).subscribeOn(Schedulers.newThread());
    }

    /**
     * Обновление смещения картинок при скроллинге вниз
     * @param newOffset смещение на новой страничке
     */
    public void updateOffset(Observable<Integer> newOffset) {
        newOffset.subscribeOn(Schedulers.newThread()).subscribe(offset -> {
            offsetHolder = offset;
        });
    }

    /**
     * Метод проверяющий есть ли картиночки дальше
     * @return the boolean на следующей "странице"
     */
    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }
}
