package apps.scvh.com.deviantapi.frontend.activities.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import apps.scvh.com.deviantapi.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
