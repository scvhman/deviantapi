package apps.scvh.com.deviantapi.frontend.activities;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;

import com.jakewharton.rxbinding2.view.RxMenuItem;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import javax.inject.Inject;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.depinj.Injector;
import apps.scvh.com.deviantapi.frontend.activities.settings.SettingsActivity;
import apps.scvh.com.deviantapi.frontend.search.SearchFacade;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviantSearch extends AppCompatActivity {

    // Даггер i баттернайф не можуть iнжектити в приватнi поля
    @Inject
    SearchFacade searchFacade;

    @BindView(R.id.deviant_results)
    RecyclerView recyclerView;
    @BindView(R.id.deviant_refresher)
    SwipyRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deviant_search);
        ButterKnife.bind(this);
        Injector.inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.deviant_menu, menu);
        searchMenuInit(menu);
        RxMenuItem.clicks(menu.findItem(R.id.settings_menu)).subscribe(click -> startActivity(new
                Intent(this, SettingsActivity.class)));
        return true;
    }

    private void searchMenuInit(Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(true);
        searchView.setFocusableInTouchMode(true);
        searchView.requestFocusFromTouch();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchFacade.initSearch(refreshLayout, recyclerView, searchView);
    }

}
