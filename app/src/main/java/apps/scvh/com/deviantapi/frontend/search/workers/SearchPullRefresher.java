package apps.scvh.com.deviantapi.frontend.search.workers;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import apps.scvh.com.deviantapi.R;
import apps.scvh.com.deviantapi.controllers.viewloaders.AdapterWorker;
import apps.scvh.com.deviantapi.core.DeviantSearchManager;
import apps.scvh.com.deviantapi.frontend.searchlist.DeviantSearchAdapter;
import apps.scvh.com.deviantapi.system.NetworkConnectivityManager;
import apps.scvh.com.deviantapi.system.SettingsManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SearchPullRefresher {

    private DeviantSearchManager searchHelper;
    private NetworkConnectivityManager connectivityManager;
    private AdapterWorker worker;
    private DeviantSearchAdapter adapter;
    private Context context;
    private SettingsManager settingsManager;

    //Опять много аргументов
    public SearchPullRefresher(DeviantSearchManager searchHelper, NetworkConnectivityManager
            connectivityManager, AdapterWorker worker, DeviantSearchAdapter adapter, Context
                                       context, SettingsManager settingsManager) {
        this.searchHelper = searchHelper;
        this.connectivityManager = connectivityManager;
        this.worker = worker;
        this.adapter = adapter;
        this.context = context;
        this.settingsManager = settingsManager;
    }

    public void autoPullInit(SwipyRefreshLayout refreshLayout, RecyclerView recyclerView) {
        refreshLayout.setOnRefreshListener(arg -> settingsManager.isApiKeyPresent().observeOn(AndroidSchedulers.mainThread()).subscribe
                (isPresented -> {
                    if (isPresented) {
                        connectivityManager.checkConnectivityAndRun(()
                                -> searchHelper.updateSearchResults().observeOn(AndroidSchedulers.mainThread()).subscribe(updated -> {
                            {   //чек на наличие картинок далее по поиску
                                if (searchHelper.isHasNext()) {
                                    //аналогичной поиску
                                    worker.addImagesToAdapter(updated, adapter, recyclerView);
                                    searchHelper.updateOffset(Observable.just(updated.getNextOffset()));
                                    searchHelper.setHasNext(Boolean.valueOf(updated.getHasMore()));
                                } else {
                                    Toast.makeText(context, context.getString(R.string
                                            .no_more), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }));
                    } else {
                        Toast.makeText(context, context.getString(R.string.no_key), Toast.LENGTH_SHORT).show();
                    }
                    refreshLayout.setRefreshing(false);
                }));
    }
}
