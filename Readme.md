# Привет!

## Из того что требовалось есть по сути все. Поиск работает(для поиска юзаю Retrofit + GSON + OkHTTP), загрузка работает(Picasso), все асинхронно(шедулеры RxДжавы ;) ), поворот работает. Есть  Львиная доля кода сделана на Rx и лямбдах. В коде есть джавадоки и комментарии на случай если нужно будет понять как что-то работает. Тестики на жюните я не завёз.

## Из того что не особо получилось - генерация Oauth2 токена в самой аппликухе, поэтому апи токен надо вводить ручками - нужен токен с scope = browse. За способ хранения ключа не ругать, я не специально ;)

### Структура папочек

#### core - собственно "ядро" отвечающее за вызовы к апи и получение оттуда данных
#### controllers - классы служащие прослойкой между ядром и отображением
#### frontend - активити, адаптеры и прочая фигня для отображения
#### system - классы для получения доступа к "системным" штукам(настройки аппликухи, проверка на наличие коннекта к инету, конвертация времени)
#### depinj - внедрение зависимостей(Dagger 2)

